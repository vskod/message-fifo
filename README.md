# message-fifo

## Installing the Development Environment

### Raspberry Pi

```bash
sudo apt-get install raspberrypi-kernel-headers
```

## Build module

```
make
```

## Load and try out

```
sudo insmod message-fifo.ko
lsmod | grep message
sudo rmmod message_fifo
dmesg | tail -100 | grep message.fifo
```

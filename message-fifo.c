#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Viacheslav Sobolev <vskod@kallkod.fi>");
MODULE_DESCRIPTION("Message-oriented FIFO");
MODULE_VERSION("0.1");

static const char * const DEVICE_NAME = "message-fifo";
#define DEVICE_COUNT 1
static dev_t region = -1;
static struct class *pclass = 0;
static struct device *pdev = 0;
static struct cdev c_dev;
static struct kobject *kobj;

static ssize_t version_show(struct device * dev,
			    struct device_attribute * attr,
			    char *buf)
{
	return scnprintf(buf, PAGE_SIZE, "%s\n", "v0.1");
}
static DEVICE_ATTR_RO(version);

static struct attribute * module_attrs[] = {
	&dev_attr_version.attr,
	NULL
};
static struct attribute_group sysfs_group = {
	.attrs = module_attrs
};

#define BUFFER_SIZE 1024
struct mf_private {
	wait_queue_head_t read;
	wait_queue_head_t write;
	struct mutex rw_mutex;
	char buffer[BUFFER_SIZE];
	size_t length;
} static private_data;

static void init_private_data(void)
{
	init_waitqueue_head(&private_data.read);
	init_waitqueue_head(&private_data.write);
	mutex_init(&private_data.rw_mutex);
}

static int mf_open(struct inode *node, struct file *file)
{
	printk(KERN_INFO"message-fifo: open\n");
	try_module_get(THIS_MODULE);
	return 0;
}

static ssize_t mf_read(struct file *file, char *buffer, size_t length, loff_t *offset)
{
	int ret;
	unsigned long n;

	if (length < private_data.length)
		return -EINVAL;

	ret = wait_event_interruptible(private_data.read, private_data.length > 0);
	if (ret)
		return ret;

	mutex_lock(&private_data.rw_mutex);
	length = private_data.length;
	n = copy_to_user(buffer, private_data.buffer, length);
	if (!n)
		private_data.length = 0;
	mutex_unlock(&private_data.rw_mutex);

	wake_up_all(&private_data.write);
	return n == 0 ? length : -ENOMEM;
}

static ssize_t mf_write(struct file *file, const char *buffer, size_t length, loff_t *offset)
{
	int ret;
	unsigned long n;

	if (length > sizeof(private_data.buffer))
		return -EBUSY;

	ret = wait_event_interruptible(private_data.write, private_data.length == 0);
	if (ret)
		return ret;

	mutex_lock(&private_data.rw_mutex);
	n = copy_from_user(private_data.buffer, buffer, length);
	if (!n)
	{
		private_data.length = length;
		wake_up_all(&private_data.read);
	}
	mutex_unlock(&private_data.rw_mutex);

	return n == 0 ? length : -ENOMEM;
}

static int mf_close(struct inode *node, struct file *file)
{
	printk(KERN_INFO"message-fifo: close\n");
	module_put(THIS_MODULE);
	return 0;
}

static struct file_operations file_ops = {
	.open  = mf_open,
	.read  = mf_read,
	.write = mf_write,
	.release = mf_close,
};

int init_module(void)
{
	int ret;

	printk(KERN_INFO"message-fifo: init\n");

	pclass = class_create(THIS_MODULE, "message-fifo");
	if (!pclass || IS_ERR(pclass))
		return -ENOMEM;

	ret = alloc_chrdev_region(&region, 0, DEVICE_COUNT, DEVICE_NAME);
	if (ret)
		goto release_class;

	pdev = device_create(pclass, NULL, region, NULL, DEVICE_NAME);
	if (!pdev)
		goto release_region;

	cdev_init(&c_dev, &file_ops);
	if (cdev_add(&c_dev, region, 1))
		goto release_device;

	kobj = kobject_create_and_add("device", &pdev->kobj);
	if (!kobj)
		goto release_device;

	ret = sysfs_create_group(kobj, &sysfs_group);
	if (ret)
		goto release_kobject;

	init_private_data();
	return 0;

release_kobject:
	kobject_put(kobj);

release_device:
	cdev_del(&c_dev);
	device_destroy(pclass, region);

release_region:
	unregister_chrdev_region(region, DEVICE_COUNT);

release_class:
	class_destroy(pclass);

	return ret;
}

void cleanup_module(void)
{
	sysfs_remove_group(kobj, &sysfs_group);
	kobject_put(kobj);
	cdev_del(&c_dev);
	device_destroy(pclass, region);
	unregister_chrdev_region(region, DEVICE_COUNT);
	class_destroy(pclass);
	printk(KERN_INFO"message-fifo: exit\n");
}

